use parser::grammar::build;
use parser::test_aux::get_test_grammar_1;

#[test]
fn main() {
    let nodes = get_test_grammar_1();
    build(&nodes);
}
