#[derive(Debug)]
pub struct Node {
    pub name: String,
    pub children: Vec<Child>,
}

#[derive(Debug)]
pub enum Child {
    Node(Node),
    Value(String),
}
