mod builder;
#[cfg(test)]
mod test;

pub use self::builder::build;

use std::collections::HashMap;
use parser::grammar::parts::*;

pub type PossibleActions = Vec<Action>;
pub type ActionTable = HashMap<State, Vec<PossibleActions>>;

#[derive(Eq, PartialEq, Hash, Debug, Clone)]
pub struct State {
    pub node_stack_head: NodeName,
    pub next_node_pattern: Pattern,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Action {
    /// shift node onto the node stack
    Shift(NodeName),
    /// move token from the input stream to the item pool
    Move(Pattern),
    /// discard 1 item from the top of the pool
    Discard,
}
