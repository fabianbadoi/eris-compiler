use std::collections::HashMap;

use super::*;
use parser::grammar::parts::*;

pub fn build(nodes: &Vec<Node>) -> ActionTable {
    let builder = Builder::new(nodes);

    builder.build()
}

struct Builder<'a> {
    nodes: HashMap<&'a str, &'a Node>,
    action_table: ActionTable,
    memo: HashMap<&'a str, Vec<Pattern>>,
}

impl<'a> Builder<'a> {
    pub fn new(nodes: &'a Vec<Node>) -> Builder {
        let mut node_map: HashMap<&str, &Node> = HashMap::new();
        for node in nodes {
            node_map.insert(&node.name, &node);
        }

        Builder {
            nodes: node_map,
            action_table: ActionTable::new(),
            memo: HashMap::new(),
        }
    }

    pub fn build(mut self) -> ActionTable {
        self.visit("ROOT");

        self.action_table
    }

    fn visit(&mut self, node_name: &'a str) -> Vec<Pattern> {
        if let Some(patterns) = self.memo.get(node_name) {
            return patterns.clone();
        }

        // prevent loops
        self.memo.insert(node_name, vec![]);

        let node = self.nodes.get(node_name)
            .expect(&format!("Node named {} not found", node_name))
            .clone();

        let mut patterns = match node.kind {
            NodeKind::Terminal(ref pattern) => {
                self.visit_terminal(node_name, pattern.clone())
            }
            NodeKind::NonTerminal(ref variants) => {
                self.visit_non_terminal(node_name, variants)
            }
        };

        patterns.sort();
        patterns.dedup();

        self.memo.insert(node_name, patterns.clone());

        patterns
    }

    fn add_entry(&mut self, head: String, next_node: Pattern, actions: PossibleActions) {
        let state = State {
            node_stack_head: head,
            next_node_pattern: next_node,
        };

        if !self.action_table.contains_key(&state) {
            self.action_table.insert(state.clone(), Vec::new());
        }

        let possibilities = self.action_table.get_mut(&state).unwrap();
        possibilities.push(actions);
    }

    fn visit_terminal(&mut self, node_name: &str, pattern: Pattern) -> Vec<Pattern> {
        let actions = vec![Action::Move(pattern.clone())];//, Action::Reduce(1)];
        self.add_entry(node_name.to_owned(), pattern.clone(), actions);

        vec![pattern]
    }

    fn visit_non_terminal(&mut self, node_name: &str, variants: &'a Variants) -> Vec<Pattern> {
        let mut all_start_patterns: Vec<Pattern> = Vec::new();

        for variant in variants {
            let mut start_patterns = self.visit_non_terminal_variant(node_name, variant);
            all_start_patterns.append(&mut start_patterns);
        }

        all_start_patterns
    }

    fn visit_non_terminal_variant(&mut self, node_name: &str, variant: &'a Variant) -> Vec<Pattern> {
        let actions = Self::get_variant_actions(variant);
        let start_patterns = self.visit_children(variant);

        for pattern in &start_patterns {
            self.add_entry(node_name.to_owned(), pattern.clone(), actions.clone());
        }

        start_patterns
    }

    fn get_variant_actions(variant: &Variant) -> Vec<Action> {
        use parser::grammar::parts::ChildNode::*;

        let actions: Vec<_> = variant.iter().fold(vec![], |mut v, child| {
            match child {
                &Literal(ref pattern) => {
                    v.push(Action::Move(pattern.to_owned()));
                    v.push(Action::Discard);
                }
                &OtherNode(ref name) => {
                    v.push(Action::Shift(name.to_owned()));
                    // v.push(Action::Reduce(1));
                },
            }

            v
        });

        actions
    }

    fn visit_children(&mut self, variant: &'a Variant) -> Vec<Pattern> {
        let first_child = &variant[0];
        let start_nodes = self.visit_child_node(first_child);

        for subsequent_child in variant.iter().skip(1) {
            self.visit_child_node(subsequent_child);
        }

        start_nodes
    }

    fn visit_child_node(&mut self, child_node: &'a ChildNode) -> Vec<Pattern> {
        use parser::grammar::parts::ChildNode::*;

        let start_patterns = match child_node {
            &Literal(ref pattern) => vec![pattern.to_owned()],
            &OtherNode(ref name) => self.visit(name),
        };

        start_patterns
    }
}
