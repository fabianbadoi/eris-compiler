use super::*;
use super::Action::*;

use parser::test_aux::*;

fn state(head: &str, next: &str) -> State {
    State { node_stack_head: s(head), next_node_pattern: s(next) }
}

#[test]
#[should_panic(expected = "Node named ROOT not found")]
fn panics_on_empty_grammar() {
    build(&Vec::new());
}

#[test]
#[should_panic(expected = "Node named ROOT not found")]
fn panics_on_missing_root() {
    build(&vec![
        rule!(NOT_ROOT)
    ]);
}

#[test]
#[should_panic(expected = "Node named MISSING not found")]
fn panics_on_missing_node() {
    build(&vec![
       rule!(ROOT <- MISSING)
    ]);
}

#[test]
fn build_root_only_table() {
    let action_table = build(&vec![
        rule!(ROOT)
    ]);

    assert_eq!(action_table.len(), 0);
}

#[test]
fn build_simple_grammar() {
    let action_table = build(&vec![
        rule!(ROOT <- NOT_ROOT),
        terminal!(NOT_ROOT = "END")
    ]);

    assert_eq!(action_table.len(), 2);
    assert_eq!(
        *action_table.get(&state("ROOT", "END")).unwrap(),
        vec![vec![Shift(s("NOT_ROOT"))]]
    );
    assert_eq!(
        *action_table.get(&state("NOT_ROOT", "END")).unwrap(),
        vec![vec![Move(s("END"))]]
    );
}

#[test]
fn build_grammar_with_multiple_options() {
    let action_table = build(&vec![
        rule!(ROOT <- NOT_ROOT, NOT_ROOT | NOT_ROOT),
        terminal!(NOT_ROOT = "END"),
    ]);

    assert_eq!(action_table.len(), 2);
    assert_eq!(
        *action_table.get(&state("NOT_ROOT", "END")).unwrap(),
        vec![vec![Move(s("END"))]]
    );

    let root_action = action_table.get(&state("ROOT", "END")).unwrap();
    assert_contains!(root_action, &vec![Shift(s("NOT_ROOT"))]);
    ;
    assert_contains!(root_action, &vec![Shift(s("NOT_ROOT")), Shift(s("NOT_ROOT"))]);
}

#[test]
fn build_test_grammar_1() {
    macro_rules! has_actions {
        ($table:expr, $head:expr, $actions:expr) => {
            for (next, expected_possibilities) in $actions {
                let state = &state($head, next);

                assert_contains_key!($table, state);

                let actual_possibilities = $table.get(state).unwrap();
                assert_eq!(expected_possibilities.len(), actual_possibilities.len());

                for possible_actions in expected_possibilities {
                    assert_contains!(actual_possibilities, &possible_actions);
                }
            }
        }
    }

    let at = &build(&get_test_grammar_1());

    assert_eq!(at.len(), 26);

    has_actions!(at, "ROOT", vec![
        ("print", vec![vec![Shift(s("LINES"))]]),
        ("pretty_print", vec![vec![Shift(s("LINES"))]]),
        ("[a-z][a-z0-9_]*", vec![vec![Shift(s("LINES"))]]),
    ]);

    has_actions!(at, "LINES", vec![
        ("print", vec![
            vec![Shift(s("LINE"))],
            vec![Shift(s("LINE")), Shift(s("LINES"))]
        ]),
        ("pretty_print", vec![
            vec![Shift(s("LINE"))],
            vec![Shift(s("LINE")), Shift(s("LINES"))]
        ]),
        ("[a-z][a-z0-9_]*", vec![
            vec![Shift(s("LINE"))],
            vec![Shift(s("LINE")), Shift(s("LINES"))]
        ]),
    ]);

    has_actions!(at, "LINE", vec![
        ("print", vec![vec![Shift(s("F_CALL")), Move(s(";")), Discard]]),
        ("pretty_print", vec![vec![Shift(s("F_CALL")), Move(s(";")), Discard]]),
        ("[a-z][a-z0-9_]*", vec![vec![Shift(s("ASSIGNMENT")), Move(s(";")), Discard]]),
    ]);

    has_actions!(at, "F_CALL", vec![
        ("print", vec![vec![Shift(s("F_NAME")), Shift(s("EXPRESSION"))]]),
        ("pretty_print", vec![vec![Shift(s("F_NAME")), Shift(s("EXPRESSION"))]]),
    ]);

    has_actions!(at, "F_NAME", vec![
        ("pretty_print", vec![vec![Shift(s("F_PP"))]]),
        ("print", vec![vec![Shift(s("F_P"))]]),
    ]);

    has_actions!(at, "F_P", vec![
        ("print", vec![vec![Move(s("print"))]]),
    ]);

    has_actions!(at, "F_PP", vec![
        ("pretty_print", vec![vec![Move(s("pretty_print"))]]),
    ]);

    has_actions!(at, "ASSIGNMENT", vec![
        ("[a-z][a-z0-9_]*", vec![vec![Shift(s("IDENTIFIER")), Move(s("=")), Discard, Shift(s("EXPRESSION"))]]),
    ]);

    has_actions!(at, "EXPRESSION", vec![
        ("[a-z][a-z0-9_]*", vec![vec![Shift(s("OPERAND"))], vec![Shift(s("OPERAND")), Shift(s("OPERATION")), Shift(s("OPERAND"))]]),
        ("[0-9]+", vec![vec![Shift(s("OPERAND"))], vec![Shift(s("OPERAND")), Shift(s("OPERATION")), Shift(s("OPERAND"))]]),
    ]);

    has_actions!(at, "OPERAND", vec![
        ("[a-z][a-z0-9_]*", vec![vec![Shift(s("IDENTIFIER"))]]),
        ("[0-9]+", vec![vec![Shift(s("INTEGER"))]]),
    ]);

    has_actions!(at, "IDENTIFIER", vec![
        ("[a-z][a-z0-9_]*", vec![vec![Move(s("[a-z][a-z0-9_]*"))]]),
    ]);

    has_actions!(at, "INTEGER", vec![
        ("[0-9]+", vec![vec![Move(s("[0-9]+"))]]),
    ]);

    has_actions!(at, "OPERATION", vec![
        ("\\+", vec![vec![Shift(s("PLUS_SIGN"))]]),
        ("-", vec![vec![Shift(s("MINUS_SIGN"))]]),
    ]);

    has_actions!(at, "PLUS_SIGN", vec![
        ("\\+", vec![vec![Move(s("\\+"))]]),
    ]);

    has_actions!(at, "MINUS_SIGN", vec![
        ("-", vec![vec![Move(s("-"))]]),
    ]);
}
