mod types;
mod mutation;

use std::rc::Rc;

use self::types::*;
use parser::input_tree::*;
use parser::grammar::state_machine::mutation::Mutation;
use super::action_table::{ActionTable, PossibleActions};

pub struct StateMachine {
    state: State,
    actions: Rc<ActionTable>,
}

impl StateMachine {
    pub fn new(action_table: ActionTable, input: String) -> Self {
        StateMachine {
            state: State::new(input),
            actions: Rc::new(action_table),
        }
    }

    pub fn run(mut self) -> Option<Child> { // TODO should be Result<Node, Errors>
        use self::mutation::Shift;
        self.state.push_to_stack(
            StackFrame::Any(Shift::new("ROOT".to_owned(), self.actions.clone(), 0))
        );

        while self.state.get_stack_len() > 0 {
            let mutation = self.state.pop_stack().unwrap();
            if !mutation.apply(&mut self.state) {
                self.unwind();
            }
        }

        self.state.take_last_objects(1).pop()
    }

    fn unwind(&mut self) {
        self.state.set_unwinding(true);

        while self.state.get_stack_len() > 0 {
            {
                match self.state.get_stack_head().unwrap() {
                    &StackFrame::SavePoint(_) => break,
                    _ => {},
                }
            }

            self.state.pop_stack();
        }
    }
}
