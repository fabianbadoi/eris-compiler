use parser::input_tree::{ Child };
use parser::grammar::parts::{ Pattern };
use super::mutation;
use super::mutation::Mutation;

pub struct State {
    stack: Vec<StackFrame>,
    objects: Vec<Child>,
    input: Input,
    unwinding: bool
}

impl State {
    pub fn new(input: String) -> Self {
        State {
            stack: Vec::new(),
            objects: Vec::new(),
            input: Input::new(input),
            unwinding: false
        }
    }

    pub fn get_snapshot(&self) -> Snapshot {
        Snapshot::new(self.objects.len(), self.input.get_position())
    }

    pub fn get_stack_len(&self) -> usize {
        self.stack.len()
    }

    pub fn restore_snapshot(&mut self, snapshot: &Snapshot) {
        self.objects.split_off(snapshot.object_count);
        self.input.rewind_to(snapshot.input_position);
    }

    pub fn get_stack_head(&self) -> Option<&StackFrame> {
        self.stack.last()
    }

    pub fn pop_stack(&mut self) -> Option<StackFrame> {
        self.stack.pop()
    }

    pub fn push_to_stack(&mut self, frame: StackFrame) {
        self.stack.push(frame);
    }

    pub fn append_to_stack(&mut self, frames: &mut Vec<StackFrame>) {
        self.stack.append(frames);
    }

    pub fn input_matches(&self, pattern: &Pattern) -> bool {
        self.input.matches(pattern)
    }

    pub fn take_input(&mut self, pattern: &Pattern) -> Option<String> {
        self.input.take(pattern)
    }

    pub fn push_object(&mut self, child: Child) {
        self.objects.push(child);
    }

    pub fn take_last_objects(&mut self, count: usize) -> Vec<Child> {
        if count > self.objects.len() {
            return Vec::new();
        }
        
        let pivot = self.objects.len() - count;

        self.objects.split_off(pivot)
    }

    pub fn get_nr_objects(&self) -> usize {
        self.objects.len()
    }

    pub fn set_unwinding(&mut self, unwinding: bool) {
        self.unwinding = unwinding;
    }

    pub fn is_unwinding(&self) -> bool {
        self.unwinding
    }
}

struct Input {
    text: String,
    position: usize,
}

impl Input {
    pub fn new(input: String) -> Input {
        Input { text: input, position: 0 }
    }

    pub fn take(&mut self, pattern: &Pattern) -> Option<String>{
        use regex::Regex;

        let regex = Regex::new(&format!("^{}\\s*", pattern)).unwrap();
        let capture_result = regex.captures(&self.text[self.position..]);

        match capture_result {
            Some(captures) => {
                let token = captures.get(0).unwrap().as_str();
                self.position += token.len();

                Some(token.trim().to_owned())
            },
            _ => None
        }
    }

    pub fn matches(&self, pattern: &Pattern) -> bool {
        use regex::Regex;

        let regex = Regex::new(&format!("^{}", pattern)).unwrap();

        regex.is_match(&self.text[self.position..])
    }

    pub fn rewind_to(&mut self, position: usize) {
        self.position = position;
    }

    pub fn get_position(&self) -> usize {
        self.position
    }
}


#[derive(Debug)]
pub enum StackFrame {
    Any(Box<mutation::Mutation>),
    SavePoint(Box<mutation::Retry>),
}

impl Mutation for StackFrame {
    fn apply(&self, state: &mut State) -> bool {
        match self {
            &StackFrame::Any(ref mutation) => mutation.apply(state),
            &StackFrame::SavePoint(ref save) => save.apply(state),
        }
    }
}

#[derive(Debug)]
pub struct Snapshot {
    object_count: usize,
    input_position: usize,
}

impl Snapshot {
    pub fn new(object_count: usize, input_position: usize) -> Snapshot {
        Snapshot { object_count, input_position }
    }

    #[cfg(test)]
    pub fn to_tuple(&self) -> (usize, usize) {
        (self.object_count, self.input_position)
    }
}
