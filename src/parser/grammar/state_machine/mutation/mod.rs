mod aux;
mod shift;
mod move_mutation;
mod reduce;
mod discard;
mod retry;

use std::fmt::Debug;

pub use self::shift::Shift;
pub use self::move_mutation::Move;
pub use self::reduce::Reduce;
pub use self::discard::Discard;
pub use self::retry::Retry;

use super::*;
use parser::grammar::parts::NodeName;

pub trait Mutation: Debug {
    fn apply(&self, state: &mut State) -> bool;

    #[cfg(test)]
    fn to_shift(&self) -> Option<&Shift> { None }
    #[cfg(test)]
    fn to_move(&self) -> Option<&Move> { None }
    #[cfg(test)]
    fn to_reduce(&self) -> Option<&Reduce> { None }
    #[cfg(test)]
    fn to_discard(&self) -> Option<&Discard> { None }
    #[cfg(test)]
    fn to_retry(&self) -> Option<&Retry> { None }
}
