use super::*;
use parser::grammar::action_table::{Action, PossibleActions};

pub fn count_nodes_in_actions(actions: &PossibleActions) -> usize {
    let shift_action_count = actions.iter()
        .filter(|action| match *action { &Action::Shift(_) => true, _ => false } )
        .count();

    if shift_action_count > 0 { shift_action_count } else { 1 }
}

pub fn make_mutation(action_table: Rc<ActionTable>, action: &Action) -> Box<Mutation> {
    match action {
        &Action::Shift(ref node_name) => Shift::new(node_name.to_owned(), action_table, 0),
        &Action::Move(ref pattern) => Move::new(pattern.to_owned()),
        &Action::Discard => Discard::new(),
    }
}
