use super::*;

/// Takes child_count items from the object pool and places a new object with those as its children
/// onto the object pool
#[derive(Debug)]
pub struct Reduce {
    name: NodeName,
    child_count: usize
}

impl Mutation for Reduce {
    fn apply(&self, state: &mut State) -> bool {
        let nr_objects = state.get_nr_objects();

        if nr_objects < self.child_count {
            return false;
        }

        let node = self.consume_objects(state);
        state.push_object(node);

        return true;
    }
}

impl Reduce {
    pub fn new(name: NodeName, child_count: usize) -> Box<Reduce> {
        Box::new(Reduce { name, child_count })
    }

    fn consume_objects(&self, state: &mut State) -> Child {
        let children = state.take_last_objects(self.child_count);
        let node = Node { name: self.name.clone(), children };

        Child::Node(node)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn fails_if_not_enough_children() {
        let mut state = State::new("".to_owned());
        state.push_object(Child::Value("child".to_owned()));

        let impossible_reduce = Reduce::new("impossible".to_owned(), 3);

        let reduce_failed = !impossible_reduce.apply(&mut state);

        assert!(reduce_failed);
    }

    #[test]
    fn consumes_objects() {
        let mut state = State::new("".to_owned());
        state.push_object(Child::Value("child".to_owned()));
        state.push_object(Child::Value("child".to_owned()));
        state.push_object(Child::Value("child".to_owned()));

        let reduce = Reduce::new("node".to_owned(), 3);

        let reduce_succeeded = reduce.apply(&mut state);

        assert!(reduce_succeeded);
        assert_eq!(1, state.get_nr_objects());
    }

    #[test]
    fn produces_desired_node() {
        let mut state = State::new("".to_owned());
        state.push_object(Child::Value("child".to_owned()));
        state.push_object(Child::Value("child".to_owned()));
        state.push_object(Child::Value("child".to_owned()));

        let reduce = Reduce::new("node".to_owned(), 3);

        reduce.apply(&mut state);

        let child = state.take_last_objects(1).pop().unwrap();
        let produced_desired_node = match child {
            Child::Node(node) => node.name == "node" && node.children.len() == 3,
            _ => false,
        };

        assert!(produced_desired_node);
    }
}
