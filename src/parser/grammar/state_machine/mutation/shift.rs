use std::rc::Rc;
use super::*;

use super::super::*;
use super::aux::*;
use parser::grammar::parts::NodeName;

#[derive(Debug)]
pub struct Shift {
    name: NodeName,
    action_table: Rc<ActionTable>,
    try_variant: usize,
}

/// Expands a node onto the stack
impl Mutation for Shift {
    fn apply(&self, state: &mut State) -> bool {
        let possibilities = self.get_possibilities(state);
        if possibilities.len() == 0 {
            return false;
        }

        self.push_mutations(state, possibilities);

        return true;
    }

    #[cfg(test)]
    fn to_shift(&self) -> Option<&Shift> {
        Some(self)
    }
}

impl Shift {
    pub fn new(name: NodeName, action_table: Rc<ActionTable>, try_variant: usize) -> Box<Shift> {
        Box::new(Shift { name, action_table, try_variant })
    }

    fn get_possibilities(&self, current_state: &State) -> Vec<PossibleActions> {
        let mut all_possible_actions = Vec::new();

        for (state, possibilities) in self.action_table.as_ref() {
            if state.node_stack_head == self.name && current_state.input_matches(&state.next_node_pattern) {
                all_possible_actions.append(&mut possibilities.clone());
            }
        }

        all_possible_actions
    }

    fn push_mutations(&self, state: &mut State, all_possibilities: Vec<PossibleActions>) {
        let (first_possibility, future_possibilities) = self.split_possibilities(all_possibilities);

        if future_possibilities.len() > 0 {
            self.push_retry(state);
        }

        self.push_reduction(state, &first_possibility);
        Self::push_action_mutations(state, &first_possibility, &self.action_table);
    }

    fn split_possibilities(&self, mut all_possibilities: Vec<PossibleActions>)
        -> (PossibleActions, Vec<PossibleActions>) {
        let mut possibilities = all_possibilities.split_off(self.try_variant);
        let first_possibility = possibilities.remove(0);

        (first_possibility, possibilities)
    }

    fn push_retry(&self, state: &mut State) {
        let retry = Retry::new(
            self.name.to_owned(),
            state.get_snapshot(),
            self.action_table.clone(),
            self.try_variant + 1
        );
        state.push_to_stack(StackFrame::SavePoint(retry));
    }

    fn push_reduction(&self, state: &mut State, actions: &PossibleActions) {
        let nr_children = count_nodes_in_actions(&actions);
        let reduce = Reduce::new(self.name.clone(), nr_children);
        state.push_to_stack(StackFrame::Any(reduce));
    }

    fn push_action_mutations(
        state: &mut State,
        actions: &PossibleActions,
        action_table: &Rc<ActionTable>
    ) {
        let mut mutations : Vec<StackFrame> =
            actions.iter()
                .map(|m| make_mutation(action_table.clone(), m))
                .map(|mutation| StackFrame::Any(mutation))
                .collect();
        mutations.reverse();

        state.append_to_stack(&mut mutations);
    }

    #[cfg(test)]
    pub fn get_node_name(&self) -> &str {
        &self.name
    }

    #[cfg(test)]
    pub fn get_variant(&self) -> usize {
        self.try_variant
    }
}
