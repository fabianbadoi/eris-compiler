use super::*;

/// Removes the last object from the object pool
#[derive(Debug)]
pub struct Discard {}

impl Discard {
    pub fn new() -> Box<Discard> {
        Box::new(Discard {})
    }
}

impl Mutation for Discard {
    fn apply(&self, state: &mut State) -> bool {
        let head = state.take_last_objects(1);
        head.len() > 0
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn fails_if_not_enough_items() {
        let mut state = State::new("".to_owned());
        let impossible_discard = Discard::new();

        let discard_failed = !impossible_discard.apply(&mut state);

        assert!(discard_failed);
    }

    #[test]
    fn discards_one_item() {
        let mut state = State::new("".to_owned());
        state.push_object(Child::Value("".to_owned()));
        state.push_object(Child::Value("".to_owned()));

        let discard = Discard::new();

        let discard_succeeded = discard.apply(&mut state);
        let nr_objects_after_discard = state.get_nr_objects();

        assert!(discard_succeeded);
        assert_eq!(1, nr_objects_after_discard);
    }
}
