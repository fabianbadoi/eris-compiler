use super::*;

/// If the state machine is unwinding after an unsuccesfful operation, tries another set of
/// possibilities
#[derive(Debug)]
pub struct Retry {
    node: NodeName,
    snapshot: Snapshot,
    action_table: Rc<ActionTable>,
    try_variant: usize,
}

impl Retry {
    pub fn new(
        node: NodeName,
        snapshot: Snapshot,
        action_table: Rc<ActionTable>,
        try_variant: usize
    ) -> Box<Retry> {
        Box::new(Retry { node, snapshot, action_table, try_variant })
    }
}

impl Mutation for Retry {
    fn apply(&self, state: &mut State) -> bool {
        if !state.is_unwinding() {
            return true;
        }

        state.restore_snapshot(&self.snapshot);
        state.push_to_stack(StackFrame::Any(Shift::new(
            self.node.clone(), self.action_table.clone(), self.try_variant
        )));
        state.set_unwinding(false);

        return true;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn does_nothing_if_unwinding() {
        let mut state = State::new("".to_owned());
        state.set_unwinding(false);

        let retry = Retry::new(
            "node".to_owned(), Snapshot::new(100, 200), Rc::new(ActionTable::new()), 0
        );

        retry.apply(&mut state);

        let current_snapshot = state.get_snapshot();

        assert_eq!((0, 0), current_snapshot.to_tuple());
    }

    #[test]
    fn restores_the_snapshot() {
        let mut state = State::new("".to_owned());
        state.push_object(Child::Value("".to_owned()));
        state.push_object(Child::Value("".to_owned()));
        state.push_object(Child::Value("".to_owned()));
        state.push_object(Child::Value("".to_owned()));

        state.set_unwinding(true);
        let mut unwinding_state = state;

        let retry = Retry::new(
            "node".to_owned(), Snapshot::new(1, 200), Rc::new(ActionTable::new()), 0
        );

        retry.apply(&mut unwinding_state);

        let current_snapshot = unwinding_state.get_snapshot();

        assert_eq!((1, 200), current_snapshot.to_tuple());
    }

    #[test]
    fn pushes_shift_mutation_onto_stack() {
        let mut state = State::new("".to_owned());
        state.set_unwinding(true);
        let mut unwinding_state = state;

        let retry = Retry::new(
            "node".to_owned(), Snapshot::new(0, 200), Rc::new(ActionTable::new()), 3
        );

        retry.apply(&mut unwinding_state);

        let stack_head = unwinding_state.pop_stack().unwrap();
        let stack_head_is_desired_shift = match stack_head {
            StackFrame::Any(mutation) => match mutation.to_shift() {
                Some(shift) => shift.get_node_name() == "node" && shift.get_variant() == 3,
                _ => false
            },
            _ => false,
        };

        assert!(stack_head_is_desired_shift);
    }

    #[test]
    fn stops_unwinding() {
        let mut state = State::new("".to_owned());
        state.set_unwinding(true);

        let retry = Retry::new(
            "node".to_owned(), Snapshot::new(0, 200), Rc::new(ActionTable::new()), 3
        );

        retry.apply(&mut state);

        assert!(!state.is_unwinding());
    }
}
