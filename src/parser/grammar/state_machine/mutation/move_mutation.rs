use parser::grammar::parts::Pattern;
use parser::input_tree::Child;

use super::super::State;
use super::Mutation;

/// Moves a pattern from the input to the object pool
#[derive(Debug)]
pub struct Move {
    pattern: Pattern
}

impl Move {
    pub fn new(pattern: Pattern) -> Box<Move> {
        Box::new(Move { pattern })
    }
}

impl Mutation for Move {
    fn apply(&self, state: &mut State) -> bool {
        let token = state.take_input(&self.pattern);

        if token.is_none() {
            return false;
        }

        state.push_object(Child::Value(token.unwrap()));

        return true;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_fails_if_it_cant_get_the_pattern() {
        let mut state = State::new("+".to_owned());
        let impossible_move = Move::new("0".to_owned());

        let apply_failed = !impossible_move.apply(&mut state);

        assert!(apply_failed);
        assert_eq!(0, state.get_nr_objects());
    }

    #[test]
    fn test_places_a_value_in_the_object_pool() {
        let mut state = State::new("0".to_owned());
        let mv = Move::new("0".to_owned());

        let apply_succeeded = mv.apply(&mut state);

        assert!(apply_succeeded);
        assert_eq!(1, state.get_nr_objects());

        let new_object = state.take_last_objects(1).pop().unwrap();
        let new_object_is_value_child = match new_object {
            Child::Value(_) => true,
            _ => false,
        };
        assert!(new_object_is_value_child);

        let object_payload = match new_object {
            Child::Value(text) => text,
            _ => "".to_owned(),
        };
        assert_eq!("0", &object_payload);
    }
}
