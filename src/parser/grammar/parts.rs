pub type NodeName = String;
pub type Pattern = String;

#[derive(Debug)]
pub struct Node {
    pub name: NodeName,
    pub kind: NodeKind,
}

#[derive(Debug)]
pub enum NodeKind {
    Terminal(Pattern),
    NonTerminal(Variants),
}

pub type Variants = Vec<Variant>;
pub type Variant = Vec<ChildNode>;

#[derive(Debug)]
pub enum ChildNode {
    Literal(Pattern),
    OtherNode(NodeName),
}
