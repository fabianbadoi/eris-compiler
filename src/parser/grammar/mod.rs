mod action_table;
mod state_machine;
pub mod parts;


use self::action_table::build as build_action_table;
use self::parts::Node;
use self::state_machine::StateMachine;


pub fn build(nodes: &Vec<Node>) {
    let action_table = build_action_table(nodes);
    let machine = StateMachine::new(
        action_table,
        "a = 3;print a;".to_owned()
    );

    let result = machine.run();
}
