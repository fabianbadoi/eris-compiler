#[macro_export]
macro_rules! rule {
    ($dest:ident <- $( $( $r:pat ),* )|*) => {{
        use parser::grammar::parts::{Node, NodeKind};

        Node {
            name: stringify!($dest).to_owned(),
            kind: NodeKind::NonTerminal(non_terminal_children!($( $( $r ),* )|*))
        }
    }};
    ($node:ident) => {{
        use parser::grammar::parts::{Node, NodeKind};

        Node {
            name: stringify!($node).to_owned(),
            kind: NodeKind::NonTerminal(vec![])
        }
    }}
}

#[macro_export]
macro_rules! terminal {
    ($dest:ident = $pattern:expr) => {{
        use parser::grammar::parts::{Node, NodeKind};

        Node {
            name: stringify!($dest).to_owned(),
            kind: NodeKind::Terminal($pattern.to_owned())
        }
    }}
}

macro_rules! non_terminal_children {
    ($a:pat | $( $( $r:pat ),* )|*) => {{
        let mut list = non_terminal_children!($a);
        let mut tail = non_terminal_children!($( $( $r ),* )|*);

        list.append(&mut tail);
        list
    }};
    ($a:pat, $( $( $r:pat ),* )|*) => {{
        let mut rest = non_terminal_children!($( $( $r ),* )|*);
        let mut old_head = rest.remove(0);
        let mut new_head = non_terminal_children!($a).remove(0);

        new_head.append(&mut old_head);
        rest.insert(0, new_head);

        rest
    }};
    ($a:pat) => {{
        use parser::grammar::parts::ChildNode;

        let string_value = stringify!($a);
        let child = if string_value.starts_with("\"") && string_value.ends_with("\"") {
            ChildNode::Literal(string_value[1..string_value.len() - 1].to_owned())
        } else {
            ChildNode::OtherNode(string_value.to_owned())
        };

        vec![vec![child]]
    }};
}
