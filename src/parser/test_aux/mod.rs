mod expect;

pub use self::expect::Expect;
use parser::grammar::parts::*;

#[allow(dead_code)]
pub fn s(s: &str) -> String { s.to_owned() }

#[macro_export]
macro_rules! assert_contains {
    ($v:expr, $i:expr) => {
        assert!(
            $v.contains($i),
            "\nElement not found in vector.\nExpected: {:?}\nFound: {:?}",
            $i,
            $v
        )
    }
}

#[macro_export]
macro_rules! assert_contains_key {
    ($h:expr, $k:expr) => {
        assert!(
            $h.contains_key($k),
            "\nHashMap does not contain key.\nKey: {:?}\nFound: {:#?}\n",
            $k,
            $h.keys()
        );
    }
}

pub fn get_test_grammar_1() -> Vec<Node> {
    let mut nodes: Vec<Node> = vec![
        rule!(ROOT <- LINES),
        rule!(LINES <- LINE, LINES | LINE),
        rule!(LINE <- F_CALL, ";" | ASSIGNMENT, ";"),
        rule!(F_CALL <- F_NAME, EXPRESSION),
        rule!(F_NAME <- F_PP | F_P),
        rule!(EXPRESSION <- OPERAND | F_CALL | OPERAND, OPERATION, OPERAND),
        rule!(OPERAND <- IDENTIFIER | INTEGER),
        rule!(OPERATION <- PLUS_SIGN | MINUS_SIGN),
        rule!(ASSIGNMENT <- IDENTIFIER, "=", EXPRESSION),
        terminal!(F_PP = "pretty_print"),
        terminal!(F_P = "print"),
        terminal!(PLUS_SIGN = "\\+"),
        terminal!(MINUS_SIGN = "-"),
        terminal!(INTEGER = "[0-9]+"),
        terminal!(IDENTIFIER = "[a-z][a-z0-9_]*"),
    ];

    nodes.reverse();

    nodes
}
