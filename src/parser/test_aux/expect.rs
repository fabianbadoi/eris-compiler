use std::fmt::Debug;
use std::cell::Cell;
use std::thread::panicking;

pub struct Expect<T, R> {
    name: String,
    will_return: Vec<(T, R)>,
    position: Cell<usize>,
}

impl<T, R> Expect<T, R>
    where T: Debug, T: Eq,
          R: Clone {
    pub fn new(name: &str, will_return: Vec<(T, R)>) -> Expect<T, R> {
        Expect { name: name.to_owned(), will_return, position: Cell::new(0) }
    }

    pub fn call(&self, param: T) -> R {
        let position = self.position.get();
        let next_call = self.will_return.get(position);

        assert!(next_call.is_some(), "{} expected to be called {} times, called {} times",
                self.name, self.will_return.len(), position + 1);

        let next_call = next_call.unwrap();

        assert_eq!(next_call.0, param, "{} expected to be called with {:?} instead of {:?}",
                   self.name, next_call.0, param);

        self.position.set(position + 1);

        next_call.1.clone()
    }
}

impl<T, R> Drop for Expect<T, R> {
    fn drop(&mut self) {
        if panicking() {
            return; // another assertion must have already failed
        }

        if self.will_return.len() != self.position.get() {
            panic!("{} expected to be called {} times, called {} times",
                   self.name, self.will_return.len(), self.position.get());
        }
    }
}

mod test {
    use super::Expect;

    #[test]
    fn matches_first_param() {
        let expect = Expect::new("test", vec![(1, "two")]);

        assert_eq!("two", expect.call(1));
    }

    #[test]
    fn matches_multiple_calls() {
        let expect = Expect::new("test", vec![(1, 11), (2, 22), (3, 33)]);

        assert_eq!(11, expect.call(1));
        assert_eq!(22, expect.call(2));
        assert_eq!(33, expect.call(3));
    }

    #[test]
    #[should_panic(expected = "test expected to be called with 1 instead of 100")]
    fn fails_on_other_params() {
        let expect = Expect::new("test", vec![(1, 2)]);

        expect.call(100);
    }

    #[test]
    #[should_panic(expected = "test expected to be called with \"first\" instead of \"second\"")]
    fn fails_on_out_of_order_calls() {
        let expect = Expect::new("test", vec![("first", 11), ("second", 22)]);

        assert_eq!(22, expect.call("second"));
        assert_eq!(11, expect.call("first"));
    }

    #[test]
    #[should_panic(expected = "test expected to be called 1 times, called 2 times")]
    fn fails_on_too_many_calls() {
        let expect = Expect::new("test", vec![(1, 2)]);

        assert_eq!(2, expect.call(1));
        assert_eq!(2, expect.call(1));
    }

    #[test]
    #[should_panic(expected = "test expected to be called 3 times, called 2 times")]
    fn fails_on_too_few_calls() {
        let expect = Expect::new("test", vec![(1, 11), (2, 22), (3, 33)]);

        assert_eq!(11, expect.call(1));
        assert_eq!(22, expect.call(2));
    }
}
